variable "environment" {
  default = "PRD"
}

variable "app_name" {
  default = ""
}

variable "cert_domain_name" {
  default = "*.domain.com"
}

variable "origin_dns_name" {
  default = ""
}

variable "cnames" {
  type    = "list"
  default = ["domain.com", "*.domain.com"]
}
