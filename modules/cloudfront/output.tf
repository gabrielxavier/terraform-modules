output "cloudfront-dns" {
  value = "${aws_cloudfront_distribution.cloudfront.domain_name}"
}
