# Rule HTTP
resource "aws_lb_listener_rule" "host_based_routing_http" {
  listener_arn = "${var.alb_listener_http_arn}"

  action {
    type             = "forward"
    target_group_arn = "${var.alb_target_group_arn}"
  }

  condition {
    field  = "host-header"
    values = ["${var.host_forward}"]
  }
}

# Rule HTTPS
resource "aws_lb_listener_rule" "host_based_routing_https" {
  listener_arn = "${var.alb_listener_https_arn}"

  action {
    type             = "forward"
    target_group_arn = "${var.alb_target_group_arn}"
  }

  condition {
    field  = "host-header"
    values = ["${var.host_forward}"]
  }
}
