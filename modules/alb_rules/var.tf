variable "host_forward" {
  type    = "list"
  default = []
}

variable "alb_target_group_arn" {
  default = ""
}

variable "alb_listener_http_arn" {
  default = ""
}

variable "alb_listener_https_arn" {
  default = ""
}
