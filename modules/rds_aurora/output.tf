output "rds_password" {
  value = "${var.rds_password == "" ?  random_string.password.result : var.rds_password}"
}

output "rds_username" {
  value = "${aws_rds_cluster.aurora_cluster.master_username}"
}

output "rds_endpoint" {
  value = "${aws_rds_cluster.aurora_cluster.endpoint}"
}
