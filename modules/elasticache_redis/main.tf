# Security Group
resource "aws_security_group" "elasticache_security_group" {
  name   = "${var.app_name}-${var.environment}-redis-sg"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group_rule" "allow_redis" {
  type        = "ingress"
  from_port   = "6379"
  to_port     = "6379"
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.elasticache_security_group.id}"
}

resource "aws_security_group_rule" "egress_allow_all" {
  type        = "egress"
  from_port   = 0
  to_port     = 65535
  protocol    = "all"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.elasticache_security_group.id}"
}

# Parameter Group
resource "aws_elasticache_parameter_group" "redis-parameters" {
  name   = "${var.app_name}-${var.environment}-redis"
  family = "redis4.0"

  parameter {
    name  = "activerehashing"
    value = "yes"
  }

  parameter {
    name  = "min-slaves-to-write"
    value = "0"
  }

  parameter {
    name  = "cluster-enabled"
    value = "yes"
  }
}

# Subnet
resource "aws_elasticache_subnet_group" "redis_subnet" {
  name       = "redis-cache-subnet"
  subnet_ids = ["${var.subnets_ids}"]
}

# Redis Instance
resource "aws_elasticache_replication_group" "default" {
  replication_group_id          = "${var.app_name}-${var.environment}"
  replication_group_description = "redis-replication-${var.environment}-${var.app_name}"
  node_type                     = "${var.redis_node_type}"
  port                          = 6379
  parameter_group_name          = "default.redis4.0.cluster.on"
  snapshot_retention_limit      = 5
  snapshot_window               = "00:00-05:00"
  subnet_group_name             = "${aws_elasticache_subnet_group.redis_subnet.name}"
  automatic_failover_enabled    = true
  apply_immediately             = true
  security_group_ids            = ["${aws_security_group.elasticache_security_group.id}"]

  cluster_mode {
    replicas_per_node_group = "${var.redis_replicas_per_node_group}"
    num_node_groups         = "${var.redis_node_groups}"
  }
}

provider "aws" {
  access_key = ""
  secret_key = ""
  region     = "us-east-1"
}
