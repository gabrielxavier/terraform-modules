variable "vpc_id" {
  default = ""
}

variable "subnets_ids" {
  type    = "list"
  default = ["sub1", "sub2"]
}

variable "app_name" {
  default = "app1"
}

variable "environment" {
  default = "PRD"
}

variable "redis_cluster_id" {
  default = "redis-name"
}

variable "redis_node_type" {
  default = "cache.t2.micro"
}

variable "redis_node_groups" {
  default = "1"
}

variable "redis_replicas_per_node_group" {
  default = "1"
}
