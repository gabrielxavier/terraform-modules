output "redis_address" {
  value = "${aws_elasticache_replication_group.default.configuration_endpoint_address}"
}
