# Roles and Policy
resource "aws_iam_instance_profile" "asg_iam_role" {
  name_prefix = "${var.asg-name}-${var.environment}-iam"
  role        = "${aws_iam_role.asg_iam_role.name}"
}

resource "aws_iam_role" "asg_iam_role" {
  name_prefix = "${var.asg-name}-${var.environment}-iam"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "asg_iam_role" {
  name_prefix = "${var.asg-name}-${var.environment}-iam"
  role        = "${aws_iam_role.asg_iam_role.id}"

  policy = "${var.asg-iam-policy}"
}

# Security Group
resource "aws_security_group" "sg_asg" {
  vpc_id      = "${var.vpc_id}"
  name_prefix = "${var.asg-name}-${var.environment}-sg"
  description = "${var.asg-name}-${var.environment}-sg"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.asg-name}-${var.environment}-sg"
  }
}

# # Latest Amazon Linux AMI
# data "aws_ami" "aws-ami" {
#   filter {
#     name   = "name"
#     values = "${var.asg-ami}"
#   }

#   most_recent = true
# }

# AutoScaling 
resource "aws_launch_configuration" "app-asg" {
  name_prefix          = "${var.asg-name}-${var.environment}"
  image_id             = "${var.asg-ami}"
  instance_type        = "${var.asg-ec2-type}"
  security_groups      = ["${aws_security_group.sg_asg.id}", "${var.security_group}"]
  key_name             = "${var.asg-key}"
  user_data            = "${var.asg-user-data}"
  iam_instance_profile = "${aws_iam_instance_profile.asg_iam_role.id}"

  root_block_device = [
    {
      volume_size = "${var.asg-disk-size}"
      volume_type = "gp2"
    },
  ]

  # ebs_block_device = [
  #     {
  #       device_name           = "/dev/xvdz"
  #        volume_type           = "gp2"
  #        volume_size           = "50"
  #        delete_on_termination = true
  #      },
  #    ]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "scale-group" {
  name_prefix          = "${var.asg-name}-${var.environment}"
  launch_configuration = "${aws_launch_configuration.app-asg.name}"
  vpc_zone_identifier  = ["${var.asg-subnets}"]
  min_size             = "${var.asg-min-instances}"
  max_size             = "${var.asg-max-instances}"
  desired_capacity     = "${var.asg-desired-instances}"
  enabled_metrics      = ["GroupMinSize", "GroupMaxSize", "GroupDesiredCapacity", "GroupInServiceInstances", "GroupTotalInstances"]
  metrics_granularity  = "1Minute"
  health_check_type    = "EC2"

  # load_balancers = ["${aws_elb.elb-web.id}"]
  # target_group_arns = ["${aws_alb_target_group.web1.arn}"]
  # health_check_type = "ELB"

  timeouts {
    delete = "15m"
  }
  tag {
    key                 = "Name"
    value               = "${var.asg-name}"
    propagate_at_launch = true
  }
  tag {
    key                 = "Environment"
    value               = "${var.environment}"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "autopolicy" {
  name                   = "${var.asg-name}-${var.environment}-policy"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 60
  autoscaling_group_name = "${aws_autoscaling_group.scale-group.name}"
}

resource "aws_cloudwatch_metric_alarm" "cpualarm" {
  alarm_name          = "${var.asg-name}-${var.environment}-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Maximum"
  threshold           = "80"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.scale-group.name}"
  }

  alarm_description = "This metric monitor EC2 instance cpu utilization"
  alarm_actions     = ["${aws_autoscaling_policy.autopolicy.arn}"]
}

resource "aws_autoscaling_policy" "autopolicy-down" {
  name                   = "${var.asg-name}-${var.environment}-autopolicy-down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 60
  autoscaling_group_name = "${aws_autoscaling_group.scale-group.name}"
}

resource "aws_cloudwatch_metric_alarm" "cpualarm-down" {
  alarm_name          = "${var.asg-name}-${var.environment}-alarm-down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Maximum"
  threshold           = "20"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.scale-group.name}"
  }

  alarm_description = "This metric monitor EC2 instance cpu utilization"
  alarm_actions     = ["${aws_autoscaling_policy.autopolicy-down.arn}"]
}
