# Certificate

data "aws_acm_certificate" "cert-ssl" {
  domain      = "${var.cert_domain_name}"
  statuses    = ["ISSUED"]
  most_recent = true
}

resource "aws_lb_listener_certificate" "cert" {
  listener_arn    = "${var.https_listener_arns}"
  certificate_arn = "${data.aws_acm_certificate.cert-ssl.arn}"
}
