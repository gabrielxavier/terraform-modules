variable "vpc_id" {}

variable "rds_subnet_ids" {
  type = "list"
}

variable "app_name" {
  default = ""
}

variable "db_name" {
  default = ""
}

variable "rds_username" {
  default = "admin"
}

variable "rds_password" {
  default = ""
}

variable "rds_diskspace" {
  default = "30"
}

variable "environment" {
  default = "prd"
}

variable "rds_instance_type" {
  default = "db.t2.micro"
}

variable "rds_engine" {
  default = "mysql"
}

variable "rds_engine_version" {
  default = "5.5.59"
}

variable "rds_multi_az" {
  default = "False"
}

variable "rds_skip_final_snapshot" {
  default = "False"
}

variable "rds_publicly_accessible" {
  default = "False"
}

variable "security_group" {
  type = "list"
}

variable "backup_retention_period" {
  default = "14"
}
