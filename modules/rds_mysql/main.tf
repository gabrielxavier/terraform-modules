# Security Group
resource "aws_security_group" "sg_rds" {
  vpc_id      = "${var.vpc_id}"
  name_prefix = "${var.app_name}-${var.environment}-sg"
  description = "${var.app_name}-${var.environment}-sg"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.app_name}-${var.environment}-sg"
  }
}

# Parameters Group
resource "aws_db_parameter_group" "parameters-rds" {
  name_prefix = "${var.app_name}-${var.environment}"
  family      = "mysql5.5"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }
}

# Subnet
resource "aws_db_subnet_group" "rds_subnet" {
  name_prefix = "${var.app_name}-${var.environment}"
  subnet_ids  = ["${var.rds_subnet_ids}"]

  tags {
    Name = "${var.app_name}-${var.environment}"
  }
}

# Generate Password
resource "random_string" "password" {
  length  = 16
  special = false
}

# RDS Instance
resource "aws_db_instance" "rds" {
  allocated_storage         = "${var.rds_diskspace}"
  storage_type              = "gp2"
  engine                    = "${var.rds_engine}"
  engine_version            = "${var.rds_engine_version}"
  instance_class            = "${var.rds_instance_type}"
  name                      = "${replace(var.db_name, "/[^a-zA-Z0-9]/", "")}"
  identifier                = "${var.app_name}-${var.environment}-db"
  username                  = "${var.rds_username}"
  password                  = "${random_string.password.result}"
  password                  = "${var.rds_password == "" ?  random_string.password.result : var.rds_password}"
  multi_az                  = "${var.rds_multi_az}"
  backup_retention_period   = "${var.backup_retention_period}"
  db_subnet_group_name      = "${aws_db_subnet_group.rds_subnet.id}"
  parameter_group_name      = "${aws_db_parameter_group.parameters-rds.name}"
  skip_final_snapshot       = "${var.rds_skip_final_snapshot}"
  publicly_accessible       = "${var.rds_publicly_accessible}"
  final_snapshot_identifier = "${var.app_name}-final-snapshot-${md5(timestamp())}"
  apply_immediately         = "true"

  vpc_security_group_ids = [
    "${aws_security_group.sg_rds.id}",
    "${var.security_group}",
  ]

  tags {
    central_custo = "${var.app_name}"
  }
}
