# Target Group
resource "aws_alb_target_group" "alb-tg" {
  name                 = "${var.tg_name}-${var.environment}"
  port                 = 80
  protocol             = "HTTP"
  vpc_id               = "${var.vpc_id}"
  deregistration_delay = 0

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    path                = "/nginx-health"
  }

  tags {
    Name        = "${var.tg_name}"
    Environment = "${var.environment}"
  }
}

resource "aws_lb_target_group_attachment" "alb-tga" {
  target_group_arn = "${aws_alb_target_group.alb-tg.arn}"
  target_id        = "${var.ec2-id}"
  port             = 80
}
