variable "vpc_id" {
  default = ""
}

variable "environment" {
  default = "PRD"
}

variable "tg_name" {
  default = ""
}

variable "ec2-id" {
  default = ""
}
