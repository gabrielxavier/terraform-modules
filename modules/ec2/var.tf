variable "app_name" {}

variable "subnet_id" {}

variable "vpc_id" {}

variable "key_pair" {}

variable "instance_type" {
  default = "t2.small"
}

variable "environment" {
  default = "PRD"
}

variable "ec2_ami" {
  default = ""
}

variable "ec2_disk_size" {
  default = "40"
}

variable "number_of_instances" {
  default = 1
}

# EC2 BootScript
variable "ec2-user-data" {
  default = ""
}

# EC2 Role
variable "ec2-iam-policy" {
  default = ""
}

variable "allow_ssh" {
  default = false
}

variable "allow_http" {
  default = false
}

variable "public_ip" {
  default = false
}

variable "attach_eip" {
  default = false
}

variable "security_group" {
  type = "list"
}
