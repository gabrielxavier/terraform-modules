data "aws_region" "current" {}

# Roles and Policy
resource "aws_iam_instance_profile" "ec2_iam_role" {
  name_prefix = "${var.app_name}-${var.environment}-iam"
  role        = "${aws_iam_role.ec2_iam_role.name}"
}

resource "aws_iam_role" "ec2_iam_role" {
  name_prefix = "${var.app_name}-${var.environment}-iam"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ec2_iam_role" {
  name_prefix = "${var.app_name}-${var.environment}-iam"
  role        = "${aws_iam_role.ec2_iam_role.id}"

  policy = "${var.ec2-iam-policy}"
}

# Security Group
resource "aws_security_group" "instance_security_group" {
  name_prefix = "${var.app_name}-${var.environment}-ec2-sg"
  vpc_id      = "${var.vpc_id}"
}

resource "aws_security_group_rule" "allow_ssh" {
  count       = "${var.allow_ssh ? 1 : 0}"
  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.instance_security_group.id}"
}

resource "aws_security_group_rule" "allow_http" {
  count       = "${var.allow_http ? 1 : 0}"
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.instance_security_group.id}"
}

resource "aws_security_group_rule" "egress_allow_all" {
  type        = "egress"
  from_port   = 0
  to_port     = 65535
  protocol    = "all"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.instance_security_group.id}"
}

# # Latest Amazon Linux AMI
# data "aws_ami" "ec2-ami" {
#   filter {
#     name   = "name"
#     values = "${var.ec2_ami}"
#   }

#   most_recent = true
# }

# EC2
resource "aws_instance" "ec2_generic_instance" {
  count = "${var.number_of_instances}"

  ami                         = "${var.ec2_ami}"
  key_name                    = "${var.key_pair}"
  subnet_id                   = "${var.subnet_id}"
  instance_type               = "${var.instance_type}"
  ebs_optimized               = true
  disable_api_termination     = true
  associate_public_ip_address = "${var.attach_eip ? var.attach_eip : var.public_ip}"
  iam_instance_profile        = "${aws_iam_instance_profile.ec2_iam_role.id}"
  user_data                   = "${var.ec2-user-data}"

  root_block_device = [
    {
      volume_size           = "${var.ec2_disk_size}"
      volume_type           = "gp2"
      delete_on_termination = true
    },
  ]

  # ebs_block_device = [
  #     {
  #       device_name           = "/dev/xvdz"
  #        volume_type           = "gp2"
  #        volume_size           = "50"
  #        delete_on_termination = true
  #      },
  #    ]

  vpc_security_group_ids = [
    "${aws_security_group.instance_security_group.id}",
    "${var.security_group}",
  ]
  tags {
    Name          = "${var.app_name}"
    Backup        = "True"
    Retention     = "14"
    central_custo = "${var.app_name}"
  }
}

# EC2 - Elastic IP
resource "aws_eip" "instance_eip" {
  count                     = "${var.attach_eip ? 1 : 0}"
  vpc                       = true
  instance                  = "${element(aws_instance.ec2_generic_instance.*.id,count.index)}"
  associate_with_private_ip = "${element(aws_instance.ec2_generic_instance.*.private_ip,count.index)}"
}
