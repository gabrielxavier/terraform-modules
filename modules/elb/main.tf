# Certificado SSL
data "aws_acm_certificate" "cert-ssl" {
  domain      = "${var.cert_domain_name}"
  statuses    = ["ISSUED"]
  most_recent = true
}

# Security Group
resource "aws_security_group" "elb-sg" {
  vpc_id      = "${var.vpc_id}"
  name        = "${var.elb_name}-${var.environment}-sg"
  description = "${var.elb_name}-${var.environment}-sg"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.elb_name}-${var.environment}-sg"
  }
}

# Elastic Load Balancer
resource "aws_elb" "elb" {
  name_prefix     = "${var.elb_name}"
  subnets         = "${var.subnets_ids}"
  security_groups = ["${aws_security_group.elb-sg.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port      = 80
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = "${data.aws_acm_certificate.cert-ssl.arn}"
  }

  health_check {
    healthy_threshold   = 5
    unhealthy_threshold = 3
    timeout             = 5
    target              = "TCP:80"
    interval            = 30
  }

  instances                   = "${var.ec2-list}"
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
}

resource "aws_lb_cookie_stickiness_policy" "default" {
  name                     = "${var.elb_name}-lbpolicy"
  load_balancer            = "${aws_elb.elb.id}"
  lb_port                  = 80
  cookie_expiration_period = 600
}
