variable "vpc_id" {
  default = ""
}

variable "environment" {
  default = "PRD"
}

variable "elb_name" {
  default = "Web"
}

variable "cert_domain_name" {
  default = ""
}

variable "ec2-list" {
  type    = "list"
  default = ["id1", "id2"]
}

variable "subnets_ids" {
  type    = "list"
  default = ["subnet1", "subnet2"]
}
