variable "vpc_name" {
  default = ""
}

variable "environment" {
  default = ""
}

variable "zone_name" {
  default = ""
}

variable "region" {
  default = "us-east-1"
}

variable "azs" {
  default = {
    "us-east-1" = "us-east-1a,us-east-1b,us-east-1c,us-east-1d"
  }
}

variable "cidr_vpc" {
  default = "10.0.0.0/16"
}

variable "cidr_networks_bits" {
  default = "8"
}

variable "subnet_count" {
  default = "4"
}
