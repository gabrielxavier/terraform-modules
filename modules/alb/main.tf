# Certificado SSL
data "aws_acm_certificate" "cert-ssl" {
  domain      = "${var.cert_domain_name}"
  statuses    = ["ISSUED"]
  most_recent = true
}

# Security Group
resource "aws_security_group" "sg_alb" {
  vpc_id      = "${var.vpc_id}"
  name        = "${var.alb_name}-${var.environment}-sg"
  description = "${var.alb_name}-${var.environment}-sg"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.alb_name}-${var.environment}-sg"
  }
}

# Application Load Balancer
resource "aws_alb" "alb" {
  name            = "${var.alb_name}-${var.environment}"
  internal        = "${var.alb_internal}"
  security_groups = ["${aws_security_group.sg_alb.id}"]
  subnets         = ["${var.subnets_ids}"]
  idle_timeout    = 300

  tags {
    Name          = "${var.alb_name}"
    Environment   = "${var.environment}"
    central_custo = "${var.app_name}"
  }
}

resource "aws_alb_listener" "alb-https" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${data.aws_acm_certificate.cert-ssl.arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.alb-tg.arn}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "alb-http" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.alb-tg.arn}"
    type             = "forward"
  }
}

# Target Group
resource "aws_alb_target_group" "alb-tg" {
  name                 = "${var.alb_name}-${var.environment}"
  port                 = 80
  protocol             = "HTTP"
  vpc_id               = "${var.vpc_id}"
  deregistration_delay = 0

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    path                = "/nginx-health"
  }

  tags {
    Name        = "${var.alb_name}"
    Environment = "${var.environment}"
  }
}

# EC2_ID - Optional
resource "aws_lb_target_group_attachment" "alb-tga" {
  target_group_arn = "${aws_alb_target_group.alb-tg.arn}"
  target_id        = "${var.ec2-id}"
  port             = 80
}
