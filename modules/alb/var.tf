variable "vpc_id" {
  default = ""
}

variable "subnets_ids" {
  type    = "list"
  default = ["sub1", "sub2"]
}

variable "environment" {
  default = "PRD"
}

variable "alb_name" {
  default = "app"
}

variable "cert_domain_name" {
  default = ""
}

variable "alb_internal" {
  default = "false"
}

variable "ec2-id" {
  default = ""
}
