output "alb_address" {
  value = "${aws_alb.alb.dns_name}"
}

output "http_listener_arns" {
  value = "${aws_alb_listener.alb-http.arn}"
}

output "https_listener_arns" {
  value = "${aws_alb_listener.alb-https.arn}"
}

output "target_group_arns" {
  value = "${aws_alb_target_group.alb-tg.arn}"
}
