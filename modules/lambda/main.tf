# Roles and Policy
resource "aws_iam_role" "lambda_role" {
  name_prefix = "${var.app_name}-${var.environment}-lambda_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lambda_policy" {
  name_prefix = "${var.app_name}-${var.environment}-lambda_policy"
  role        = "${aws_iam_role.lambda_role.id}"

  policy = "${var.lambda_iam_policy}"
}

# Zip Files
data "archive_file" "lambda_zip" {
  type        = "zip"
  source_dir  = "${var.file_source}"
  output_path = "/tmp/function.zip"
}

# Lambda Function
resource "aws_lambda_function" "lambda_function" {
  filename         = "/tmp/function.zip"
  function_name    = "${var.app_name}"
  role             = "${aws_iam_role.lambda_role.arn}"
  handler          = "${var.handler_name}"
  source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"
  runtime          = "${var.lang}"
  memory_size      = "${var.memory}"
  timeout          = "${var.timeout}"

  tags {
    name        = "${var.app_name}"
    environment = "${var.environment}"
  }
}

# Cloudwatch Events and Schedule
resource "aws_cloudwatch_event_rule" "lambda_cloudwatch_event" {
  name_prefix         = "${var.app_name}-${var.environment}-lambda"
  description         = "Run every ${var.schedule_rate}"
  schedule_expression = "rate(${var.schedule_rate})"
}

resource "aws_cloudwatch_event_target" "lambda_cloudwatch_event_target" {
  rule      = "${aws_cloudwatch_event_rule.lambda_cloudwatch_event.name}"
  target_id = "${var.app_name}-${var.environment}-lambda"
  arn       = "${aws_lambda_function.lambda_function.arn}"
}

resource "aws_lambda_permission" "lambda_execute_permission" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda_function.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.lambda_cloudwatch_event.arn}"
}
