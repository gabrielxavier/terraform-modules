provider "aws" {
  region = "${module.environment.region}"
}

module "environment" {
  source = "../../../"
}

terraform {
  backend "s3" {
    bucket = "bucket-terraform-states"
    key    = "data/mysql_01"
    region = "us-east-1"
  }

  required_version = "> 0.11.0"
}

data "terraform_remote_state" "vpc_state" {
  backend = "s3"

  config {
    bucket = "bucket-terraform-states"
    key    = "network/vpc"
    region = "us-east-1"
  }
}

module "rds" {
  source                  = "../../../../../modules/rds_mysql"
  app_name                = "${var.db_name}"
  db_name                 = "${var.db_name}"
  rds_engine              = "${var.rds_engine}"
  rds_engine_version      = "${var.rds_engine_version}"
  rds_diskspace           = "${var.rds_diskspace}"
  rds_instance_type       = "${var.rds_instance_type}"
  rds_publicly_accessible = "${var.rds_publicly_accessible}"
  rds_multi_az            = "${var.rds_multi_az}"
  rds_password            = "${var.rds_password == "" ? "" : var.rds_password}"
  rds_subnet_ids          = "${split(",", "${var.rds_publicly_accessible ? data.terraform_remote_state.vpc_state.public_subnets : data.terraform_remote_state.vpc_state.private_subnets}")}"
  environment             = "${module.environment.environment}"
  security_group          = "${var.security_group}"
  backup_retention_period = "${var.backup_retention_period}"
  vpc_id                  = "${data.terraform_remote_state.vpc_state.vpc_id}"
}
