variable "db_name" {
  default = "mysql01"
}

variable "rds_engine" {
  default = "mysql"
}

variable "rds_engine_version" {
  default = "5.5.59"
}

variable "rds_password" {
  default = ""
}

variable "rds_instance_type" {
  default = "db.t2.medium"
}

variable "rds_multi_az" {
  default = false
}

variable "rds_publicly_accessible" {
  default = false
}

variable "rds_diskspace" {
  default = "100"
}

variable "security_group" {
  type    = "list"
  default = []
}

variable "backup_retention_period" {
  default = "30"
}
