variable "db_name" {
  default = "db-example"
}

variable "rds_engine" {
  default = "postgres"
}

variable "rds_engine_version" {
  default = "9.6"
}

variable "rds_password" {
  default = ""
}

variable "rds_instance_type" {
  default = "db.t2.micro"
}

variable "rds_multi_az" {
  default = false
}

variable "rds_publicly_accessible" {
  default = false
}

variable "rds_diskspace" {
  default = "30"
}

variable "security_group" {
  type    = "list"
  default = []
}
