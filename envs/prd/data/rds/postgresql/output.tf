output "db_password" {
  value = "${module.rds.rds_password}"
}

output "db_username" {
  value = "${module.rds.rds_username}"
}

output "db_hostname" {
  value = "${module.rds.rds_address}"
}
