provider "aws" {
  region = "${module.environment.region}"
}

terraform {
  backend "s3" {
    bucket = "bucket-terraform-states"
    key    = "data/elasticache"
    region = "us-east-1"
  }
}

module "environment" {
  source = "../../../"
}

data "terraform_remote_state" "vpc_state" {
  backend = "s3"

  config {
    bucket = "bucket-terraform-states"
    key    = "network/vpc"
    region = "us-east-1"
  }
}

module "elasticache_redis" {
  source                        = "../../../../../modules/elasticache_redis"
  app_name                      = "${var.app_name}"
  redis_cluster_id              = "${var.redis_cluster_id}"
  redis_node_type               = "${var.redis_node_type}"
  redis_node_groups             = "${var.redis_node_groups}"
  redis_replicas_per_node_group = "${var.redis_replicas_per_node_group}"
  environment                   = "${module.environment.environment}"
  subnets_ids                   = "${split(",", data.terraform_remote_state.vpc_state.private_subnets)}"
  vpc_id                        = "${data.terraform_remote_state.vpc_state.vpc_id}"
}

# module "route53" {
#   source      = "../../../../../modules/route53"
#   dns_entry   = "${var.app_name}"
#   environment = "${module.environment.environment}"
#   zone_name   = "${module.environment.zone_name}"
#   zone_id     = "${data.terraform_remote_state.vpc_state.zone_id}"
#   ip_address  = "${module.elasticache_redis.cache_nodes.0.address}"
# }

