variable "app_name" {
  default = "app1"
}

variable "redis_cluster_id" {
  default = "redis-name"
}

variable "redis_node_type" {
  default = "cache.t2.micro"
}

variable "redis_node_groups" {
  default = "1"
}

variable "redis_replicas_per_node_group" {
  default = "1"
}
