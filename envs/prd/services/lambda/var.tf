variable "environment" {
  default = "PRD"
}

variable "app_name" {
  default = "app1"
}

variable "file_source" {
  default = "./files/"
}

variable "handler_name" {
  default = "app1.lambda_handler"
}

variable "lang" {
  default = "python2.7"
}

variable "schedule_rate" {
  default = "5 minutes"
}

variable "memory" {
  default = "256"
}

variable "timeout" {
  default = "300"
}

variable "lambda_iam_policy" {
  default = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:StartInstances",
                "ec2:StopInstances",
                "ec2:DescribeRegions",
                "ec2:DescribeInstances",
                "lambda:InvokeFunction"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}
