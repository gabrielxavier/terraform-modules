provider "aws" {
  region = "${module.environment.region}"
}

terraform {
  backend "s3" {
    bucket = "bucket-terraform-states"
    key    = "services/lambda-app"
    region = "us-east-1"
  }
}

module "environment" {
  source = "../../"
}

module "lambda" {
  source            = "../../../../modules/lambda"
  environment       = "${module.environment.environment}"
  app_name          = "${var.app_name}"
  file_source       = "${var.file_source}"
  handler_name      = "${var.handler_name}"
  lang              = "${var.lang}"
  schedule_rate     = "${var.schedule_rate}"
  memory            = "${var.memory}"
  timeout           = "${var.timeout}"
  lambda_iam_policy = "${var.lambda_iam_policy}"
}
