provider "aws" {
  region = "${module.environment.region}"
}

terraform {
  backend "s3" {
    bucket = "bucket-terraform-states"
    key    = "services/webserver-asg"
    region = "us-east-1"
  }
}

module "environment" {
  source = "../../"
}

data "terraform_remote_state" "vpc_state" {
  backend = "s3"

  config {
    bucket = "bucket-terraform-states"
    key    = "network/vpc"
    region = "us-east-1"
  }
}

module "asg_alb" {
  source                = "../../../../modules/asg_alb"
  app_name              = "${var.app_name}"
  cert_domain_name      = "${var.cert_domain_name}"
  asg-ec2-type          = "${var.asg-ec2-type}"
  asg-disk-size         = "${var.asg-disk-size}"
  asg-min-instances     = "${var.asg-min-instances}"
  asg-max-instances     = "${var.asg-max-instances}"
  asg-desired-instances = "${var.asg-desired-instances}"
  asg-user-data         = "${var.asg-user-data}"
  asg-iam-policy        = "${var.asg-iam-policy}"
  asg-ami               = "${var.asg-ami}"
  environment           = "${module.environment.environment}"
  asg-key               = "${data.terraform_remote_state.vpc_state.key_name}"
  elb_subnets           = "${split(",", data.terraform_remote_state.vpc_state.public_subnets)}"
  asg-subnets           = "${split(",", data.terraform_remote_state.vpc_state.private_subnets)}"
  vpc_id                = "${data.terraform_remote_state.vpc_state.vpc_id}"
  security_group        = ["${var.security_group}", "${data.terraform_remote_state.vpc_state.vpc_security_group}"]
  alb_internal          = false
}

module "route53" {
  source = "../../../../modules/route53"

  dns_type    = "CNAME"
  dns_entry   = "${var.app_name}"
  environment = "${module.environment.environment}"
  zone_name   = "${module.environment.zone_name}"
  zone_id     = "${data.terraform_remote_state.vpc_state.zone_id}"
  ip_address  = "${module.asg_alb.dns_name}"
}
