variable "app_name" {
  default = "example"
}

variable "cert_domain_name" {
  default = "domain.com"
}

variable "asg-ec2-type" {
  default = "t2.micro"
}

variable "asg-ami" {
  default = "ami-cfe4b2b0"
}

variable "asg-disk-size" {
  default = "20"
}

variable "asg-min-instances" {
  default = "1"
}

variable "asg-max-instances" {
  default = "1"
}

variable "asg-desired-instances" {
  default = "1"
}

variable "security_group" {
  type    = "list"
  default = []
}

variable "asg-user-data" {
  default = <<EOF
  #!/bin/bash
  yum install git -y
  pip install ansible
  # sudo -u ec2-user /usr/bin/ssh-keyscan -t rsa gitlab.com > /home/ec2-user/.ssh/known_hosts
  # sudo -u ec2-user /usr/bin/aws s3 cp s3://deploy-keys/app1/id_rsa /home/ec2-user/.ssh/
  # sudo -u ec2-user /bin/chmod 400 /home/ec2-user/.ssh/id_rsa
  # cd /home/ec2-user/
  # sudo -u ec2-user git clone -b prd git@gitlab.com:deploy/ansible.git
  # cd /home/ec2-user/ansible/app1
  # sudo /usr/local/bin/ansible-playbook -i localhost provision.yml
EOF
}

variable "asg-iam-policy" {
  default = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "elasticloadbalancing:*",
                "ec2:Describe*",
                "ec2:AmazonS3FullAccess",
                "cloudfront:*",
                "autoscaling:Describe*",
                "cloudwatch:*",
                "logs:*",
                "sns:*",
                "s3:*"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}
