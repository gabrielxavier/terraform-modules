variable "app_name" {
  default = "Webserver_01"
}

variable "instance_type" {
  default = "c5.large"
}

variable "ec2_ami" {
  default = "ami-0977029b5b13f3d08"
}

variable "security_group" {
  type    = "list"
  default = []
}

variable "user_data" {
  default = <<EOF
  #!/bin/bash
  apt update -q
  apt install -qy git python-pip
  pip install boto boto3 ansible
  # sudo -u ubuntu /usr/bin/ssh-keyscan -t rsa gitlab.com > /home/ubuntu/.ssh/known_hosts
  # sudo -u ubuntu /usr/bin/aws s3 cp s3://deploy-keys/app1/id_rsa /home/ubuntu/.ssh/
  # sudo -u ubuntu /bin/chmod 400 /home/ubuntu/.ssh/id_rsa
  # cd /home/ubuntu/
  # sudo -u ubuntu git clone -b prd git@gitlab.com:deploy/ansible.git
  # cd /home/ubuntu/ansible/app1
  # sudo /usr/local/bin/ansible-playbook -i localhost provision.yml
EOF
}

variable "ec2-iam-policy" {
  default = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "elasticloadbalancing:*",
                "ec2:Describe*",
                "cloudwatch:*",
                "logs:*",
                "sns:*",
                "s3:*",
                "acm:ListCertificates",
                "cloudfront:*",
                "iam:ListServerCertificates",
                "waf:ListWebACLs",
                "waf:GetWebACL"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

variable "allow_ssh" {
  default = false
}

variable "allow_http" {
  default = false
}

variable "public_ip" {
  default = false
}

variable "attach_eip" {
  default = false
}

variable "ec2_disk_size" {
  default = 40
}
