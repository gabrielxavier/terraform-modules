provider "aws" {
  region = "${module.environment.region}"
}

terraform {
  backend "s3" {
    bucket = "bucket-terraform-states"
    key    = "services/ec2_01"
    region = "us-east-1"
  }

  required_version = "> 0.11.0"
}

data "terraform_remote_state" "vpc_state" {
  backend = "s3"

  config {
    bucket = "bucket-terraform-states"
    key    = "network/vpc"
    region = "us-east-1"
  }
}

module "environment" {
  source = "../../"
}

module "ec2" {
  source         = "../../../../modules/ec2"
  app_name       = "${var.app_name}"
  environment    = "${module.environment.environment}"
  instance_type  = "${var.instance_type}"
  key_pair       = "${data.terraform_remote_state.vpc_state.key_name}"
  subnet_id      = "${element(split(",", "${"${var.attach_eip ? var.attach_eip : var.public_ip}" ? data.terraform_remote_state.vpc_state.public_subnets : data.terraform_remote_state.vpc_state.private_subnets}"), 0)}"
  vpc_id         = "${data.terraform_remote_state.vpc_state.vpc_id}"
  security_group = ["${var.security_group}", "${data.terraform_remote_state.vpc_state.vpc_security_group}"]
  public_ip      = "${var.public_ip}"
  attach_eip     = "${var.attach_eip}"
  allow_ssh      = "${var.allow_ssh}"
  allow_http     = "${var.allow_http}"
  ec2-user-data  = "${var.user_data}"
  ec2_ami        = "${var.ec2_ami}"
  ec2-iam-policy = "${var.ec2-iam-policy}"
  ec2_disk_size  = "${var.ec2_disk_size}"
}

module "route53" {
  source      = "../../../../modules/route53"
  dns_entry   = "${var.app_name}"
  environment = "${module.environment.environment}"
  zone_name   = "${module.environment.zone_name}"
  zone_id     = "${data.terraform_remote_state.vpc_state.zone_id}"
  ip_address  = "${module.ec2.private_ip}"
}
