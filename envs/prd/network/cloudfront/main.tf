provider "aws" {
  region = "${module.environment.region}"
}

module "environment" {
  source = "../../"
}

terraform {
  backend "s3" {
    bucket = "bucket-terraform-states"
    key    = "network/cloudfront_01"
    region = "us-east-1"
  }

  required_version = "> 0.11.0"
}

data "terraform_remote_state" "alb_01" {
  backend = "s3"

  config {
    bucket = "bucket-terraform-states"
    key    = "network/alb_01"
    region = "us-east-1"
  }
}

module "cloudfront_01" {
  source           = "../../../../modules/cloudfront"
  app_name         = "test"
  cert_domain_name = "*.domain.com"
  origin_dns_name  = "${data.terraform_remote_state.alb_01.alb_01_address}"
  cnames           = ["domain.com", "*.domain.com"]
}
