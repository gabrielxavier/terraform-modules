provider "aws" {
  region = "${module.environment.region}"
}

module "environment" {
  source = "../../"
}

terraform {
  backend "s3" {
    bucket = "bucket-terraform-states"
    key    = "network/vpc"
    region = "us-east-1"
  }

  required_version = "> 0.11.0"
}

module "keypair" {
  source = "../../../../modules/keypair"

  client_name = "${module.environment.client_name}"
  environment = "${module.environment.environment}"
}

module "vpc" {
  source = "../../../../modules/vpc"

  zone_name   = "${module.environment.zone_name}"
  vpc_name    = "${var.vpc_name}"
  environment = "${module.environment.environment}"
  cidr_vpc    = "${var.cidr_vpc}"
  azs         = "${var.azs}"
}
