provider "aws" {
  region = "${module.environment.region}"
}

module "environment" {
  source = "../../"
}

terraform {
  backend "s3" {
    bucket = "bucket-terraform-states"
    key    = "network/alb_01"
    region = "us-east-1"
  }

  required_version = "> 0.11.0"
}

data "terraform_remote_state" "vpc_state" {
  backend = "s3"

  config {
    bucket = "bucket-terraform-states"
    key    = "network/vpc"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "ec2" {
  backend = "s3"

  config {
    bucket = "bucket-terraform-states"
    key    = "services/ec2_01"
    region = "us-east-1"
  }
}

module "alb_01" {
  source           = "../../../../modules/alb"
  vpc_id           = "${data.terraform_remote_state.vpc_state.vpc_id}"
  subnets_ids      = "${split(",", data.terraform_remote_state.vpc_state.public_subnets)}"
  alb_name         = "ALB-01"
  cert_domain_name = "*.domain.com"
  ec2-id           = "${data.terraform_remote_state.ec2.instance_id}"
}

# # Certificates
# module "cert_pool_01" {
#   source              = "../../../../modules/alb_certificates"
#   https_listener_arns = "${module.alb_01.https_listener_arns}"
#   cert_domain_name    = "*.domain1.com"
# }
# module "cert_pool_02" {
#   source              = "../../../../modules/alb_certificates"
#   https_listener_arns = "${module.alb_01.https_listener_arns}"
#   cert_domain_name    = "*.domain2.com"
# }


# # Target Groups
# module "tg_pool_01" {
#   source  = "../../../../modules/alb_target_groups"
#   vpc_id  = "${data.terraform_remote_state.vpc_state.vpc_id}"
#   tg_name = "TG-Pool-01"
#   ec2-id  = "${data.terraform_remote_state.ec2.instance_id}"
# }


# # Rules
# module "rule_pool_01" {
#   source                 = "../../../../modules/alb_rules"
#   alb_listener_http_arn  = "${module.alb_01.http_listener_arns}"
#   alb_listener_https_arn = "${module.alb_01.https_listener_arns}"
#   alb_target_group_arn   = "${module.alb_01.target_group_arns}"
#   host_forward           = ["www.domain.com"]
# }
# module "rule_pool_02" {
#   source                 = "../../../../modules/alb_rules"
#   alb_listener_http_arn  = "${module.alb_01.http_listener_arns}"
#   alb_listener_https_arn = "${module.alb_01.https_listener_arns}"
#   alb_target_group_arn   = "${module.alb_01.target_group_arns}"
#   host_forward           = ["test.domain.com"]
# }
# module "rule_pool_03" {
#   source                 = "../../../../modules/alb_rules"
#   alb_listener_http_arn  = "${module.alb_01.http_listener_arns}"
#   alb_listener_https_arn = "${module.alb_01.https_listener_arns}"
#   alb_target_group_arn   = "${module.tg_pool_01.target_group_arns}"
#   host_forward           = ["test2.domain.com"]
# }

