output "environment" {
  value = "prd"
}

output "region" {
  value = "us-east-1"
}

output "zone_name" {
  value = "clientname"
}

output "client_name" {
  value = "clientname"
}

output "default_security_groups" {
  value = []
}
